# Description

Sine wave generator kernel.

# Build

```sh
cmake -B build -S .
cmake --build build
./install.sh
```

# Testing 

Use CLI toolkit to execute the kernel.

Set frequency:
```
dsptool generate -k sine_wave -o sine.wav -p frequency=660 --play 
```
