#include <cmath>
#include "dspkernel.hpp"
#include "prec/sin.hpp"

class SineGeneratorKernel : public dsp::Kernel {
	double sampleRate;
	double dt;
	int inputChannels;
	int outputChannels;
	int blockSize;
	double w = 0;
	
	// Parameters
	double freq = 440;
	double gain = 1;

public:
	SineGeneratorKernel() {
		addDoubleParameter("frequency", [this](double value) { freq = value; });
		addDoubleParameter("gain", [this](double value) { gain = value; });
	}

	~SineGeneratorKernel() {}

	const int dspKernelVersion() const {
		return dsp::VERSION;
	}

	const char* getName() const {
		return "Sine wave generator";
	}

	void configurePlayback(double sampleRate, int inputChannels, int outputChannels, int blockSize) {
		this->sampleRate = sampleRate;
		this->inputChannels = inputChannels;
		this->outputChannels = outputChannels;
		this->blockSize = blockSize;
		this->dt = 1 / sampleRate;
	}

	void process(const dsp::Sample* inputs, dsp::Sample* outputs) {
		for (int i = 0; i < blockSize; i++) {
			float out = p_sin(w * 2 * M_PI * freq);

			for (int channel = 0; channel < outputChannels; channel++) {
				outputs[i * outputChannels + channel] = out;
			}

			w += dt;
		}
	}

	void noteOn(int offset, int voice, unsigned key, float value) {}
	void noteOff(int offset, int voice) {}
};

// Export kernel

extern "C" dsp::Kernel* createKernel() {
	return new SineGeneratorKernel();
}

extern "C" void destroyKernel(SineGeneratorKernel* kernel) {
	delete kernel;
}
