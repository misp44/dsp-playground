#pragma once

#include <iostream>

template <class Arg_T, class Ret_T>
class PrecalculatedFunction {
	int steps;
	const Ret_T* values;
	Arg_T begin;
	Arg_T end;
	Arg_T length;
public:
	explicit PrecalculatedFunction(
		int steps,
		const Ret_T* values,
		Arg_T begin,
		Arg_T end
	):
		steps(steps),
		values(values),
		begin(begin),
		end(end),
		length(end - begin) {}

	virtual ~PrecalculatedFunction() {}

	Ret_T operator() (Arg_T arg) {
		// TODO: create implementation using bit masks instead of modulo
		unsigned index = int((arg - begin) / length * steps) % steps;

		return values[index];
	}
};
