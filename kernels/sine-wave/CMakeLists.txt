cmake_minimum_required(VERSION 3.0.0)

project(sine_wave_kernel VERSION 0.1.0 DESCRIPTION "Sine wave generator kernel")

set(GeneratedSources "${CMAKE_CURRENT_SOURCE_DIR}/generated")

add_custom_command(
	OUTPUT "${GeneratedSources}/prec/sin.hpp"
	COMMAND node ./generateprecalculatedfunctions.js
	WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
	DEPENDS "generateprecalculatedfunctions.js"
	DEPENDS "src/generateprecalculatedfunction.js"
)

add_custom_target(run ALL DEPENDS "${GeneratedSources}/prec/sin.hpp")

add_library(sine_wave_kernel SHARED src/kernel.cpp)

target_compile_features(sine_wave_kernel PRIVATE cxx_std_17)
target_include_directories(sine_wave_kernel PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/../../dsp-kernel/include")
target_include_directories(sine_wave_kernel PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/generated")
target_include_directories(sine_wave_kernel PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/src")

