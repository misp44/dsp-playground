# Description

Delay effect kernel.

# Build

```sh
cmake -B build -S .
cmake --build build
./install.sh
```

# Testing 

Use CLI toolkit to execute the kernel.

```sh
dsptool process -k delay -f [source audio file] -o delayed.wav -t 5 -p 'delay time=0.5' -p feedback=0.6 --play
```
