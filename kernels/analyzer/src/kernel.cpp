#include <iostream>
#include <vector>
#include <cmath>
#include "dspkernel.hpp"

class AnalyzerKernel : public dsp::Kernel {
	double dt = 0;
	int inputChannels;
	int outputChannels;
	int blockSize;
	long samples = 0;
	
	// Channel specific measurements
	std::vector<double> minAmplitudeValue;
	std::vector<double> maxAmplitudeValue;
	std::vector<long double> integrals;
	std::vector<long double> squareIntegrals;

	// Mono measurements
	double monoIntegral = 0;
	long double monoSquareIntegral = 0;

	// TODO: Stereo correlation
public:
	AnalyzerKernel() {}

	~AnalyzerKernel() {
		std::cout << "\n\nMeasurements results:\n";

		for (int i = 0; i < inputChannels; i++) {
			double rms = RMS(squareIntegrals[i]);
			// level of full scale (-1 to 1):
			//   - sine wave is equal    0 dBFS
			//   - square wave is equal +3 dBFS
			double dBFS = RMS_to_dBFS(rms);

			std::cout << "Channel #" << i << std::endl;
			std::cout << "  Minimum amplitude value:  " << minAmplitudeValue[i] << std::endl;
			std::cout << "  Maxmimum amplitude value: " << maxAmplitudeValue[i] << std::endl;
			std::cout << "  Integral value:           " << integrals[i] << std::endl;
			std::cout << "  RMS levl:                 " << rms << std::endl;
			std::cout << "  dBFS level:               " << dBFS << std::endl;
		}

		double monoRms = RMS(monoSquareIntegral);
		double monodBFS = RMS_to_dBFS(monoRms);

		std::cout << "Mono:" << std::endl;
		std::cout << "  Integral value: " << monoIntegral << std::endl;
		std::cout << "  RMS levl:                 " << monoRms << std::endl;
		std::cout << "  dBFS level:               " << monodBFS << std::endl;

		// TODO: Stereo measurements
	}

	const int dspKernelVersion() const {
		return dsp::VERSION;
	}

	const char* getName() const {
		return "Signal analyzer";
	}

	void configurePlayback(double sampleRate, int inputChannels, int outputChannels, int blockSize) {
		this->inputChannels = inputChannels;
		this->outputChannels = outputChannels;
		this->blockSize = blockSize;

		dt = 1 / sampleRate;

		for (int channel = 0; channel < inputChannels; channel++) {
			minAmplitudeValue.push_back(0);
			maxAmplitudeValue.push_back(0);
			integrals.push_back(0);
			squareIntegrals.push_back(0);
		}
	}

	void process(const dsp::Sample* inputs, dsp::Sample* outputs) {
		double mono;

		for (int i = 0; i < blockSize; i++) {
			mono = 0;

			for (int channel = 0; channel < inputChannels; channel++) {
				float input = inputs[i * inputChannels + channel];
				mono += input;

				if (input < minAmplitudeValue[channel]) {
					minAmplitudeValue[channel] = input;
				}

				if (input > maxAmplitudeValue[channel]) {
					maxAmplitudeValue[channel] = input;
				}

				integrals[channel] += input * dt;
				squareIntegrals[channel] += input * input;
			}

			mono = mono / (double)inputChannels;
			monoIntegral += mono * dt;
			monoSquareIntegral += mono * mono;

			samples++;
		}

		for (int i = 0; i < blockSize * outputChannels; i++) {
			outputs[i] = 0;
		}
	}

	void noteOn(int offset, int voice, unsigned key, float value) {}
	void noteOff(int offset, int voice) {}

private:
	inline double RMS(long double squareIntegral) {
		return sqrt(squareIntegral / (long double)samples);
	}

	inline double RMS_to_dBFS(double rms) {
		constexpr double sqrtOfTwo = 1.4142135623730951;

		return 20 * log10(rms * sqrtOfTwo);
	}
};

// Export kernel

extern "C" AnalyzerKernel* createKernel() {
	return new AnalyzerKernel();
}

extern "C" void destroyKernel(AnalyzerKernel* kernel) {
	delete kernel;
}
