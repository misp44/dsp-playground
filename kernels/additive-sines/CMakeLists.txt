cmake_minimum_required(VERSION 3.0.0)

project(additive_sines_wave_kernel VERSION 0.1.0 DESCRIPTION "Sine wave generator kernel")

add_library(additive_sines_wave_kernel SHARED src/kernel.cpp)

target_compile_features(additive_sines_wave_kernel PRIVATE cxx_std_17)
target_include_directories(additive_sines_wave_kernel PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../../dsp-kernel/include)
