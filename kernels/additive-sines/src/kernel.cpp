#include <iostream>
#include <cmath>
#include "dspkernel.hpp"

const int defaultHarmonicsSize = 12;
const float defaultHarmonics[] = {0.5, 0.1, 0.3, 0.2, 0.05, 0.1, 0.02, 0.08, 0.05, 0.05, 0.05, 0.05};

class AdditiveSinesGeneratorKernel : public dsp::Kernel {
	double sampleRate;
	double dt;
	int inputChannels;
	int outputChannels;
	int blockSize;
	double w = 0;
	double harmonicsSum = 0;
	
	// Parameters
	double freq = 440;
	double gain = 1;
	dsp::Array<float> harmonics = dsp::Array<float>::from(defaultHarmonicsSize, defaultHarmonics);

public:
	AdditiveSinesGeneratorKernel() {
		addDoubleParameter("frequency", [this](double value) { freq = value; });
		addDoubleParameter("gain", [this](double value) { gain = value; });
		addFloatArrayParameter(
			"harmonics",
			[this](dsp::Array<float> array) {
				harmonics.free();
				harmonics = array;
				calculateHarmonicsSum();
			},
			[this](unsigned index, float value) { harmonics.values[index] = value; calculateHarmonicsSum(); }
		);
	}

	~AdditiveSinesGeneratorKernel() {
		harmonics.free();
	}

	const int dspKernelVersion() const {
		return dsp::VERSION;
	}

	const char* getName() const {
		return "Addive sines wave generator";
	}

	void configurePlayback(double sampleRate, int inputChannels, int outputChannels, int blockSize) {
		this->sampleRate = sampleRate;
		this->inputChannels = inputChannels;
		this->outputChannels = outputChannels;
		this->blockSize = blockSize;
		this->dt = 1 / sampleRate;
	}

	void process(const dsp::Sample* inputs, dsp::Sample* outputs) {
		for (int i = 0; i < blockSize; i++) {
			float out = additiveSine(w * 2 * M_PI * freq) * gain;

			for (int channel = 0; channel < outputChannels; channel++) {
				outputs[i * outputChannels + channel] = out;
			}

			w += dt;
		}
	}

	void noteOn(int offset, int voice, unsigned key, float value) {}
	void noteOff(int offset, int voice) {}
private:
	inline float additiveSine(float t) {
		float f = 0;

		for (int i = 1; i <= harmonics.elements; i++) {
			f += sin(t * (float)i) * (harmonics.values[i - 1] / harmonicsSum);
		}

		return f;
	}

	inline void calculateHarmonicsSum() {
		harmonicsSum = 0;

		for (int i = 0; i < harmonics.elements; i++) {
			harmonicsSum += harmonics.values[i];
		}
	}
};

// Export kernel

extern "C" dsp::Kernel* createKernel() {
	return new AdditiveSinesGeneratorKernel();
}

extern "C" void destroyKernel(AdditiveSinesGeneratorKernel* kernel) {
	delete kernel;
}
