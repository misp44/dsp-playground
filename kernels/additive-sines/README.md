# Description

Additive wave generator kernel.

# Build

```sh
cmake -B build -S .
cmake --build build
./install.sh
```

# Testing 

Use CLI toolkit to execute the kernel.

Set frequency:
```
dsptool generate -k additive_sines_wave -o asdf.wav -p 'harmonics=1;0;0;2;0.1' -p frequency=660 -P
```
