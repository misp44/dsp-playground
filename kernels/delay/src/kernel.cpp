#include <iostream>
#include <stdexcept>
#include <vector>
#include "dspkernel.hpp"

constexpr double maxDelayTime = 2;

class CircularBuffer {
	dsp::Sample* buffer;

	int size;
	int position = 0;
public:
	CircularBuffer(int size) {
		buffer = new dsp::Sample[size];
		this->size = size;
	}

	~CircularBuffer() {
		delete[] buffer;
	}

	inline void write(dsp::Sample sample) {
		position = (position - 1) >= size ? 0 : position + 1;

		buffer[position] = sample;
	}

	inline dsp::Sample read(int delay) const {
		delay = delay % size;

		int readPosition = position - delay;

		if (readPosition < 0) {
			return buffer[size + readPosition];
		}

		return buffer[readPosition];
	}
};

class DelayEffectKernel : public dsp::Kernel {
	double sampleRate;
	int inputChannels;
	int outputChannels;
	int channels;
	int blockSize;

	std::vector<CircularBuffer*> circularBuffers;
	
	// parameters
	double delayTime = 0.6;
	double feedback = 0.3;
	float delayLevel = 0.6;

	int delaySamples = 0;
public:
	DelayEffectKernel() {
		addDoubleParameter("delay time", [this](double value) {
			delayTime = value;
			delaySamples = delayTime * sampleRate;
		});
		addDoubleParameter("feedback", [this](double value) { feedback = value; });
		addFloatParameter("delay level", [this](double value) { delayLevel = value; });
	}

	~DelayEffectKernel() {
		for (CircularBuffer* buffer : circularBuffers) {
			delete buffer;
		}
	}

	const int dspKernelVersion() const {
		return dsp::VERSION;
	}

	const char* getName() const {
		return "Delay effect";
	}

	void configurePlayback(double sampleRate, int inputChannels, int outputChannels, int blockSize) {
		if (inputChannels == 0) {
			throw std::invalid_argument("No input channels defined");
		}

		this->sampleRate = sampleRate;
		this->inputChannels = inputChannels;
		this->outputChannels = outputChannels;
		this->blockSize = blockSize;

		channels = std::min(inputChannels, outputChannels);

		delaySamples = delayTime * sampleRate;

		// TODO: buffers should be deleted first
		circularBuffers.clear();
		circularBuffers.reserve(channels);

		for (int i = 0; i < channels; i++) {
			// TODO: Use ceil instead of floor
			circularBuffers.push_back(new CircularBuffer(maxDelayTime * sampleRate));
		}
	}

	void process(const dsp::Sample* inputs, dsp::Sample* outputs) {
		for (int i = 0; i < blockSize; i++) {
			for (int channel = 0; channel < inputChannels; channel++) {
				CircularBuffer* dl = circularBuffers[channel];

				dl->write(inputs[i * inputChannels + channel] + dl->read(delaySamples) * feedback);
			}

			for (int channel = 0; channel < channels; channel++) {
				dsp::Sample in = inputs[i * inputChannels + channel];
				CircularBuffer* dl = circularBuffers[channel];

				outputs[i * outputChannels + channel] = in + delayLevel* dl->read(delaySamples);
			}
		}
	}

	void noteOn(int offset, int voice, unsigned key, float value) {}
	void noteOff(int offset, int voice) {}
};

// Export kernel

extern "C" DelayEffectKernel* createKernel() {
	return new DelayEffectKernel();
}

extern "C" void destroyKernel(DelayEffectKernel* kernel) {
	delete kernel;
}
