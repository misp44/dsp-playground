#pragma once

#include <cmath>

#define DEBUG

#ifdef DEBUG
#include <iostream>
#endif

#include "dspkernel.hpp"

#include "circularbuffer.hpp"

class SchroederReverb {
	float reverbTime;
	float sampleRate;

	float comb1_LoopGain;
	float comb2_LoopGain;
	float comb3_LoopGain;
	float comb4_LoopGain;
	float allPass1_LoopGain = 0.7;
	float allPass2_LoopGain = 0.5;

	float comb1_DelayTime;
	float comb2_DelayTime;
	float comb3_DelayTime;
	float comb4_DelayTime;
	float allPass1_DelayTime;
	float allPass2_DelayTime;


	// Damping should be in range 0 to 1
	float damping = 0.1;
	float comb_lp_1 = 0;
	float comb_lp_2 = 0;
	float comb_lp_3 = 0;
	float comb_lp_4 = 0;

	int comb1_DelaySamples;
	int comb2_DelaySamples;
	int comb3_DelaySamples;
	int comb4_DelaySamples;
	int allPass1_DelaySamples;
	int allPass2_DelaySamples;

	CircularBuffer comb1_Delay;
	CircularBuffer comb2_Delay;
	CircularBuffer comb3_Delay;
	CircularBuffer comb4_Delay;
	CircularBuffer allPass1_Delay;
	CircularBuffer allPass2_Delay;
public:
	SchroederReverb(
		float sampleRate,
		float comb1_DelayTime,
		float comb2_DelayTime,
		float comb3_DelayTime,
		float comb4_DelayTime,
		float allPass1_DelayTime,
		float allPass2_DelayTime
	):
		sampleRate(sampleRate),
		comb1_DelayTime(comb1_DelayTime),
		comb2_DelayTime(comb2_DelayTime),
		comb3_DelayTime(comb3_DelayTime),
		comb4_DelayTime(comb4_DelayTime),
		allPass1_DelayTime(allPass1_DelayTime),
		allPass2_DelayTime(allPass2_DelayTime),
		comb1_DelaySamples(comb1_DelayTime * sampleRate),
		comb2_DelaySamples(comb2_DelayTime * sampleRate),
		comb3_DelaySamples(comb3_DelayTime * sampleRate),
		comb4_DelaySamples(comb4_DelayTime * sampleRate),
		allPass1_DelaySamples(allPass1_DelayTime * sampleRate),
		allPass2_DelaySamples(allPass2_DelayTime * sampleRate),
		comb1_Delay(comb1_DelaySamples + 1),
		comb2_Delay(comb2_DelaySamples + 1),
		comb3_Delay(comb3_DelaySamples + 1),
		comb4_Delay(comb4_DelaySamples + 1),
		allPass1_Delay(allPass1_DelaySamples + 1),
		allPass2_Delay(allPass2_DelaySamples + 1) {}

	void setReverbTime(float reverbTime) {
		// TODO: Why it works better "*0.0001"?
		this->reverbTime = reverbTime * 0.0001;
	}

	void calculateCoefficients() {
		comb1_LoopGain = calculateLoopGain(comb1_DelayTime);
		comb2_LoopGain = calculateLoopGain(comb2_DelayTime);
		comb3_LoopGain = calculateLoopGain(comb3_DelayTime);
		comb4_LoopGain = calculateLoopGain(comb4_DelayTime);

		#ifdef DEBUG
		std::cout << "Reverb time: " << reverbTime << "\n";
		std::cout << "C1 Loop gain: " << comb1_LoopGain << "\n";
		std::cout << "C2 Loop gain: " << comb2_LoopGain << "\n";
		std::cout << "C3 Loop gain: " << comb3_LoopGain << "\n";
		std::cout << "C4 Loop gain: " << comb4_LoopGain << "\n";
		std::cout << "AP1 Loop gain: " << allPass1_LoopGain << "\n";
		std::cout << "AP2 Loop gain: " << allPass2_LoopGain << "\n";
		std::cout << "C1 Time: " << comb1_DelaySamples << "\n";
		std::cout << "C2 Time: " << comb2_DelaySamples << "\n";
		std::cout << "C3 Time: " << comb3_DelaySamples << "\n";
		std::cout << "C4 Time: " << comb4_DelaySamples << "\n";
		std::cout << "AP1 Time: " << allPass1_DelaySamples << "\n";
		std::cout << "AP2 Time: " << allPass2_DelaySamples << "\n";
		#endif
	}

	float tick(float in) {
		float comb1_delayed = comb1_Delay.read(comb1_DelaySamples);
		float comb2_delayed = comb2_Delay.read(comb2_DelaySamples);
		float comb3_delayed = comb3_Delay.read(comb3_DelaySamples);
		float comb4_delayed = comb4_Delay.read(comb4_DelaySamples);

		// comb_lp_1 = comb1_delayed + comb_lp_1 * damping * (1 - comb1_LoopGain);
		// comb_lp_2 = comb2_delayed + comb_lp_2 * damping * (1 - comb2_LoopGain);
		// comb_lp_3 = comb3_delayed + comb_lp_3 * damping * (1 - comb3_LoopGain);
		// comb_lp_4 = comb4_delayed + comb_lp_4 * damping * (1 - comb4_LoopGain);

		comb_lp_1 = -(1-damping) * comb1_delayed + comb_lp_1 * damping;
		comb_lp_2 = -(1-damping) * comb2_delayed + comb_lp_2 * damping;
		comb_lp_3 = -(1-damping) * comb3_delayed + comb_lp_3 * damping;
		comb_lp_4 = -(1-damping) * comb4_delayed + comb_lp_4 * damping;

		float comb1_out = in + comb_lp_1 * comb1_LoopGain;
		float comb2_out = in + comb_lp_2 * comb2_LoopGain;
		float comb3_out = in + comb_lp_3 * comb3_LoopGain;
		float comb4_out = in + comb_lp_4 * comb4_LoopGain;

		comb1_Delay.write(comb1_out);
		comb2_Delay.write(comb2_out);
		comb3_Delay.write(comb3_out);
		comb4_Delay.write(comb4_out);

		
		// float combBank_out = comb1_delayed + comb2_delayed + comb3_delayed + comb4_delayed;
		float combBank_out = comb1_delayed - comb2_delayed + comb3_delayed - comb4_delayed;

		float allPass1_delay = allPass1_Delay.read(allPass1_DelaySamples);
		float allPass1_delayAndInput = combBank_out + allPass1_delay * allPass1_LoopGain;
		float allPass1_out = allPass1_delayAndInput * -allPass1_LoopGain + allPass1_delay;
		allPass1_Delay.write(allPass1_delayAndInput);

		float allPass2_delay = allPass2_Delay.read(allPass2_DelaySamples);
		float allPass2_delayAndInput = allPass1_out + allPass2_delay * allPass2_LoopGain;
		float allPass2_out = allPass2_delayAndInput * -allPass2_LoopGain + allPass2_delay;
		allPass2_Delay.write(allPass2_delayAndInput);

		return allPass2_out;
	};

private:
	float calculateLoopGain(float delayTime) {
		return std::pow(10, (-3.0f * delayTime) / (reverbTime * sampleRate));
	}
};
