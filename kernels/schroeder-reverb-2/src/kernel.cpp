#include <iostream>
#include <stdexcept>
#include <vector>
#include "dspkernel.hpp"
#include "reverb.hpp"

class SchroederReverbKernel : public dsp::Kernel {
	double sampleRate;
	int inputChannels;
	int outputChannels;
	int channels;
	int blockSize;

	std::vector<SchroederReverb*> reverbs;
	
	// Parameters
	double reverbTime = 3;
	double dry = 0.5;
	double wet = 0.5;

public:
	SchroederReverbKernel() {
		addDoubleParameter("time", [this](double value) { reverbTime = value; calculateCoefficients(); });
		addDoubleParameter("dry", [this](double value) { dry = value; });
		addDoubleParameter("wet", [this](double value) { wet = value; });
	}

	~SchroederReverbKernel() {}

	const int dspKernelVersion() const {
		return dsp::VERSION;
	}

	const char* getName() const {
		return "Simple Schroeder reverberator with low pass feedback";
	}

	void configurePlayback(double sampleRate, int inputChannels, int outputChannels, int blockSize) {
		this->sampleRate = sampleRate;
		this->inputChannels = inputChannels;
		this->outputChannels = outputChannels;
		this->blockSize = blockSize;

		channels = std::min(inputChannels, outputChannels);

		// TODO: reverbs should be deleted first (before .clear())
		reverbs.clear();
		reverbs.reserve(channels);

		for (int i = 0; i < channels; i++) {
			// Use prime numbers to avoid overlaping responses
			// [ 59, 61, 67, 71 ] * 0.0008
			reverbs.push_back(new SchroederReverb(
				sampleRate,
				59 * 0.0008,
				61 * 0.0008,
				67 * 0.0008,
				71 * 0.0008,
				0.01051,
				0.00337
			));

		}

		calculateCoefficients();
	}

	void process(const dsp::Sample* inputs, dsp::Sample* outputs) {
		for (int i = 0; i < blockSize; i++) {
			for (int channel = 0; channel < channels; channel++) {
				SchroederReverb* reverb = reverbs[channel];

				dsp::Sample in = inputs[i * inputChannels + channel];

				outputs[i * outputChannels + channel] = in * dry + reverb->tick(in) * wet;
			}
		}
	}

	void noteOn(int offset, int voice, unsigned key, float value) {}
	void noteOff(int offset, int voice) {}

private:
	void calculateCoefficients() {
		for (SchroederReverb* reverb : reverbs) {
			reverb->setReverbTime(reverbTime);
			reverb->calculateCoefficients();
		}
	}
};

// Export kernel

extern "C" dsp::Kernel* createKernel() {
	return new SchroederReverbKernel();
}

extern "C" void destroyKernel(SchroederReverbKernel* kernel) {
	delete kernel;
}
