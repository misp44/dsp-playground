#pragma once

#ifdef DEBUG
#include <iostream>
#endif
#include <stdexcept>

class CircularBuffer {
	dsp::Sample* buffer;

	int size;
	int position = 0;
public:
	explicit CircularBuffer(int size) {
		#ifdef DEBUG
		std::cout << "Initializing circular buffer, size = " << size << std::endl;
		#endif

		if (size < 1) {
			throw new std::runtime_error("Circular buffer size cannot must be greater than 1");
		}

		buffer = new dsp::Sample[size];
		this->size = size;

		for (int i = 0; i < size; i++) {
			buffer[i] = 0;
		}
	}

	~CircularBuffer() {
		delete[] buffer;
	}

	inline void write(dsp::Sample sample) {
		position = (position + 1) >= size ? 0 : position + 1;

		buffer[position] = sample;
	}

	inline dsp::Sample read(int delay) const {
		delay = delay % size;

		int readPosition = position - delay;

		if (readPosition < 0) {
			readPosition = size + readPosition;
		}

		#ifdef DEBUG
		if (readPosition < 0) {
			throw new std::runtime_error("Read position cannot be less than 0");
		}

		if (readPosition >= size) {
			throw new std::runtime_error("Read position cannot be greater or equal than circular buffer size");
		}
		#endif

		return buffer[readPosition];
	}
};
