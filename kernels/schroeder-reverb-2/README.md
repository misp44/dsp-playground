# Description

Schroeder reverb with low pass feedback.

# Build

```sh
cmake -B build -S .
cmake --build build
./install.sh
```

# Testing 

Use CLI toolkit to execute the kernel.

```
dsptool process -k schroeder_reverb_2 -f [input file] -o reverb.wav -t 6 --play
```

## Visualize impulse response

```
dsptool impulse-response -k schroeder_reverb_2 --channels 1 -o impulse-response.mat -p time=1 -p dry=0
octave impulse-response.mat
```

![Impulse response](impulse-response.svg)
