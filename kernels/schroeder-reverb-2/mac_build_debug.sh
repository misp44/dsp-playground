#!/bin/bash

cmake -B build -S . -DCMAKE_BUILD_TYPE=Debug -DCMAKE_PREFIX_PATH='/usr/local/opt/llvm' \
	-DCMAKE_CXX_COMPILER='/usr/local/opt/llvm/bin/clang++' \
	-DCMAKE_C_COMPILER='/usr/local/opt/llvm/bin/clang'
