# Description

Delay effect kernel created in [Vult language](https://www.vult-dsp.com/vult-language).

# Build

```sh
./build_vult_files.sh
cmake -B build -S .
cmake --build build
./install.sh
```

# Testing 

Use CLI toolkit to execute the kernel.

```
dsptool process -k vult_delay -f [source audio file] -o delayed.wav -t 5 -p 'delay time=0.5' -p feedback=0.6 --play
```
