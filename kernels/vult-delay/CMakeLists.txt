cmake_minimum_required(VERSION 3.0.0)

project(vult_delay_kernel VERSION 0.1.0 DESCRIPTION "Delay effect kernel built with Vult")

add_library(vult_delay_kernel SHARED src/kernel.cpp generated/processor.cpp vult-runtime/vultin.cpp)

target_compile_features(vult_delay_kernel PRIVATE cxx_std_17)
target_include_directories(vult_delay_kernel PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../../dsp-kernel/include)
target_include_directories(vult_delay_kernel PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/generated)
target_include_directories(vult_delay_kernel PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/vult-runtime)
