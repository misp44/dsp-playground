#include <stdexcept>
#include <vector>
#include "dspkernel.hpp"
#include "processor.h"

class DelayEffectKernel : public dsp::Kernel {
	int blockSize;

	Processor_delay_type context;
	
	// parameters
	double delayTime = 0.6;
	double feedback = 0.3;
	float delayLevel = 0.6;

	int delaySamples = 0;
public:
	DelayEffectKernel() {
		Processor_delay_init(context);

		Processor_setParameter(context.delayTimeParameter, 0.6);
		Processor_setParameter(context.feedbackParameter, 0.3);
		Processor_setParameter(context.delayLevelParameter, 0.6);

		addFloatParameter("delay time", [this](float value) {
			Processor_setParameter(context.delayTimeParameter, value);
		});
		addFloatParameter("feedback", [this](float value) {
			Processor_setParameter(context.feedbackParameter, value);
		});
		addFloatParameter("delay level", [this](float value) {
			Processor_setParameter(context.delayLevelParameter, value);
		});
	}

	~DelayEffectKernel() {}

	const int dspKernelVersion() const {
		return dsp::VERSION;
	}

	const char* getName() const {
		return "Delay effect (vult)";
	}

	void configurePlayback(double sampleRate, int inputChannels, int outputChannels, int blockSize) {
		if (inputChannels != 2 && outputChannels != 2) {
			throw std::invalid_argument("Only stereo signals accepted");
		}

		if (sampleRate > 96000) {
			throw std::invalid_argument("Sample rate not supported");
		}

		this->blockSize = blockSize;

		Processor_setParameter(context.sampleRateParameter, sampleRate);
	}

	void process(const dsp::Sample* inputs, dsp::Sample* outputs) {
		for (int i = 0; i < blockSize; i ++) {
			Processor_delay(context, inputs[i * 2], inputs[i * 2 + 1]);

			// Left channel
			outputs[i * 2] = Processor_delay_ret_0(context);
			// Right channel
			outputs[i * 2 + 1] = Processor_delay_ret_1(context);
		}
	}

	void noteOn(int offset, int voice, unsigned key, float value) {}
	void noteOff(int offset, int voice) {}
};

// Export kernel

extern "C" DelayEffectKernel* createKernel() {
	return new DelayEffectKernel();
}

extern "C" void destroyKernel(DelayEffectKernel* kernel) {
	delete kernel;
}
