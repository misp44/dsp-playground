cmake_minimum_required(VERSION 3.0.0)

project(dsptool VERSION 0.1.0)

set(CMAKE_TOOLCHAIN_FILE ${CMAKE_CURRENT_SOURCE_DIR}/../vcpkg/scripts/buildsystems/vcpkg.cmake CACHE STRING "Vcpkg toolchain file")

# MacOS note:  "-fsanitize=leak" is not available in clang delivered with XCode - use clang from brew
# To check app for memory leaks, set ASAN_OPTIONS=detect_leaks=1 environment variable
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O1 -g -fno-omit-frame-pointer -fsanitize=address -fsanitize=leak")
set(CMAKE_LINKER_FLAGS_DEBUG "${CMAKE_LINKER_FLAGS_DEBUG} -g -fno-omit-frame-pointer -fsanitize=address -fsanitize=leak")

include(${CMAKE_CURRENT_SOURCE_DIR}/../vcpkg/scripts/buildsystems/vcpkg.cmake)

file(GLOB_RECURSE dsptool_SRC
  "src/**/*.cpp"
)

find_path(CPPCODEC_INCLUDE_DIRS "cppcodec/base32_crockford.hpp")
find_package(cxxopts CONFIG REQUIRED)
find_package(fmt CONFIG REQUIRED)
find_package(SFML COMPONENTS system audio CONFIG REQUIRED)
# find_package(SFML COMPONENTS system window graphics CONFIG REQUIRED)
find_package(skia CONFIG REQUIRED)

add_executable(dsptool src/main.cpp ${dsptool_SRC})

target_compile_features(dsptool PRIVATE cxx_std_17)

target_include_directories(dsptool PRIVATE ${CPPCODEC_INCLUDE_DIRS})
target_include_directories(dsptool PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../dsp-kernel/include)
target_include_directories(dsptool PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src/helpers)
target_link_libraries(dsptool PRIVATE fmt::fmt)
target_link_libraries(dsptool PRIVATE cxxopts::cxxopts)
target_link_libraries(dsptool PRIVATE sfml-audio)
# target_link_libraries(main PRIVATE sfml-system sfml-network sfml-graphics sfml-window)
target_link_libraries(dsptool PRIVATE skia skia::skia)

