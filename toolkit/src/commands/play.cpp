#include <iostream>
#include <fmt/core.h>
#include <fmt/color.h>
#include <cxxopts.hpp>

#include "player.hpp"

#include "play.hpp"

const char* PlayCommand::name() {
	return "play";
}

const char* PlayCommand::description() {
	return "Play audio file";
}

int PlayCommand::execute(int argc, char** argv) {
	cxxopts::Options options(argv[0], description());

	options.add_options()
		("f,file", "Input file name", cxxopts::value<std::string>())
		("h,help", "Help")
	;

	auto result = options.parse(argc, argv);

	if (result.count("help")) {
		std::cout << options.help();

		return 0;
	}

	std::string filename = result["file"].as<std::string>();

	fmt::print(fg(fmt::color::crimson), "Playing \"{}\" file\n", filename);

	return playAudioFile(filename);
}
