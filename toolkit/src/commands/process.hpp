#pragma once

#include "command.hpp"

class ProcessCommand : public Command {
public:
	const char* name();
	const char* description();

	int execute(int argc, char** argv);
};
