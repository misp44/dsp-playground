#pragma once

#include <string>

class Command {
public:
	virtual ~Command() = default;
	virtual const char* name() = 0;
	virtual const char* description() = 0;

	virtual int execute(int argc, char** argv) = 0;
};
