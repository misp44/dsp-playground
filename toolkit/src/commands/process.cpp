#include <iostream>
#include <fmt/core.h>
#include <fmt/color.h>
#include <cxxopts.hpp>
#include <SFML/Audio.hpp>

#include "kernelprocessor.hpp"
#include "player.hpp"

#include "process.hpp"

const char* ProcessCommand::name() {
	return "process";
}

const char* ProcessCommand::description() {
	return "Process audio with specified effect kernel";
}

int ProcessCommand::execute(int argc, char** argv) {
	cxxopts::Options options(argv[0], description());

	options.add_options()
		("k,kernel", "Kernel file (path to dynamic library)", cxxopts::value<std::string>())
		("f,file", "Input audio file", cxxopts::value<std::string>())
		("o,output", "Output audio file", cxxopts::value<std::string>())
		("channels", "Output channels", cxxopts::value<int>())
		("s,sampleRate", "Sample rate", cxxopts::value<int>())
		("t,time", "Time in seconds", cxxopts::value<double>())
		("p,params", "Kernel parameters", cxxopts::value<std::vector<std::string>>())
		("P,play", "Play audio")
		("a,analyze", "Enable analyzer of output signal")
		("h,help", "Help")
	;

	auto result = options.parse(argc, argv);

	if (result.count("help")) {
		std::cout << options.help();

		return 0;
	}

	std::string filename = result["kernel"].as<std::string>();
	std::string inputFilename = result["file"].as<std::string>();
	std::string outputFilename = result["output"].as<std::string>();

	sf::InputSoundFile inputFile;
	inputFile.openFromFile(inputFilename);

	int inputChannels = inputFile.getChannelCount();

	int outputChannels = result.count("channels") ? result["channels"].as<int>() : inputChannels;
	int sampleRate = result.count("sampleRate") ? result["sampleRate"].as<int>() : inputFile.getSampleRate();
	double time = result.count("time") ? result["time"].as<double>() : inputFile.getDuration().asSeconds();

	if (sampleRate != inputFile.getSampleRate()) {
		fmt::print(
			fg(fmt::color::crimson),
			"Input sample rate ({}) is different than output({}). Playback speed can affect sound pitch.\n",
			inputFile.getSampleRate(),
			sampleRate
		);
	}

	KernelProcessor processor(filename);

	if (result.count("params")) {
		processor.applyKernelParameters(result["params"].as<std::vector<std::string>>());
	}

	if (result.count("analyze")) {
		processor.enableAnalyzer();
	}

	processor.process(sampleRate, inputChannels, outputChannels, time * sampleRate, &inputFile, outputFilename);

	if (result.count("play")) {
		return playAudioFile(outputFilename);
	}

	return 0;
}
