#include <iostream>
#include <fmt/core.h>
#include <cxxopts.hpp>

#include "dspkernel.hpp"
#include "externalkernel.hpp"

#include "listparams.hpp"

constexpr int blockSize = 1024;

const char* ListParamsCommand::name() {
	return "lsparams";
}

const char* ListParamsCommand::description() {
	return "Get list of kernel parameters";
}

int ListParamsCommand::execute(int argc, char** argv) {
	cxxopts::Options options(argv[0], description());

	options.add_options()
		("k,kernel", "Kernel file (path to dynamic library)", cxxopts::value<std::string>())
		("h,help", "Help")
	;

	auto result = options.parse(argc, argv);

	if (result.count("help")) {
		std::cout << options.help();

		return 0;
	}

	std::string filename = result["kernel"].as<std::string>();

	ExternalKernel kernel(filename);

	for (int i = 0; i < kernel.get()->getParametersNumber(); i++) {
		fmt::print("  {: >{}}:", i, 5);
		fmt::print(" {: <{}}", kernel.get()->getParameterName(i), 20);
		dsp::ParameterType parameterType = kernel.get()->getParameterType(i);
		fmt::print("({})\n", dsp::parameterTypeString(parameterType));
	}

	return 0;
}
