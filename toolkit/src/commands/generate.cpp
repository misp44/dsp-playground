#include <iostream>
#include <fmt/core.h>
#include <fmt/color.h>
#include <cxxopts.hpp>
#include <SFML/Audio.hpp>

#include "kernelprocessor.hpp"
#include "player.hpp"

#include "generate.hpp"

const char* GenerateCommand::name() {
	return "generate";
}

const char* GenerateCommand::description() {
	return "Generates audio with specified kernel";
}

int GenerateCommand::execute(int argc, char** argv) {
	cxxopts::Options options(argv[0], description());

	options.add_options()
		("k,kernel", "Kernel file (path to dynamic library)", cxxopts::value<std::string>())
		("o,output", "Output audio file", cxxopts::value<std::string>())
		("channels", "Output channels", cxxopts::value<int>()->default_value("2"))
		("s,sampleRate", "Sample rate", cxxopts::value<int>()->default_value("44100"))
		("t,time", "Time in seconds", cxxopts::value<double>()->default_value("3"))
		("p,params", "Kernel parameters", cxxopts::value<std::vector<std::string>>())
		("P,play", "Play audio")
		("a,analyze", "Enable analyzer of output signal")
		("h,help", "Help")
	;

	auto result = options.parse(argc, argv);

	if (result.count("help")) {
		std::cout << options.help();

		return 0;
	}

	std::string filename = result["kernel"].as<std::string>();
	std::string outputFilename = result["output"].as<std::string>();
	int outputChannels = result["channels"].as<int>();
	int sampleRate = result["sampleRate"].as<int>();
	double time = result["time"].as<double>();

	KernelProcessor processor(filename);

	if (result.count("params")) {
		processor.applyKernelParameters(result["params"].as<std::vector<std::string>>());
	}

	if (result.count("analyze")) {
		processor.enableAnalyzer();
	}

	// TODO: Add events file
	processor.process(sampleRate, 0, outputChannels, time * sampleRate, nullptr, outputFilename);

	if (result.count("play")) {
		return playAudioFile(outputFilename);
	}

	return 0;
}
