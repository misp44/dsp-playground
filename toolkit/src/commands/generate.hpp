#pragma once

#include "command.hpp"

class GenerateCommand : public Command {
public:
	const char* name();
	const char* description();

	int execute(int argc, char** argv);
};
