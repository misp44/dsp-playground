#include <iostream>
#include <fmt/core.h>
#include <fmt/color.h>
#include <cxxopts.hpp>
#include <SFML/Audio.hpp>

#include "kernelprocessor.hpp"
#include "player.hpp"

#include "impulseresponse.hpp"

const char* ImpulseResponseCommand::name() {
	return "impulse-response";
}

const char* ImpulseResponseCommand::description() {
	return "Generates impulse response";
}

int ImpulseResponseCommand::execute(int argc, char** argv) {
	cxxopts::Options options(argv[0], description());

	options.add_options()
		("k,kernel", "Kernel file (path to dynamic library)", cxxopts::value<std::string>())
		("o,output", "Output file in Octave format (open with \"octave [filename]\"", cxxopts::value<std::string>())
		("channels", "Output channels", cxxopts::value<int>())
		("s,sampleRate", "Sample rate", cxxopts::value<int>())
		("t,time", "Time in seconds", cxxopts::value<double>())
		("p,params", "Kernel parameters", cxxopts::value<std::vector<std::string>>())
		("P,play", "Play audio")
		("h,help", "Help")
	;

	auto result = options.parse(argc, argv);

	if (result.count("help")) {
		std::cout << options.help();

		return 0;
	}

	std::string filename = result["kernel"].as<std::string>();
	std::string outputFilename = result["output"].as<std::string>();

	int outputChannels = result["channels"].as<int>();
	int sampleRate = result.count("sampleRate") ? result["sampleRate"].as<int>() : 44100;
	double time = result.count("time") ? result["time"].as<double>() : 2;

	KernelProcessor processor(filename);

	if (result.count("params")) {
		processor.applyKernelParameters(result["params"].as<std::vector<std::string>>());
	}

	processor.generateImpulseResponse(sampleRate, outputChannels, time * sampleRate, outputFilename);

	if (result.count("play")) {
		return playAudioFile(outputFilename);
	}

	return 0;
}
