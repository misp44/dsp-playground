#pragma once

#include <fmt/core.h>
#include <string>
#include <map>
#include "command.hpp"

class Commands {
public:
	~Commands() {
		for (auto command : commands) {
			delete command.second;
		}
	}

	inline int execute(int argc, char** argv) {
		if (argc < 2) {
			printCommandsList(argv[0]);

			return 1;
		}

		if (!commands.count(argv[1])) {
			printCommandsList(argv[0]);

			return 1;
		}

		Command* command = commands[argv[1]];

		return command->execute(argc - 1, argv + 1);
	}

	inline void addCommand(Command* command) {
		commands[command->name()] = command;
	}

	inline void printCommandsList(const std::string& command) {
		fmt::print("DSP playground toolkit\n");
		fmt::print("Usage:\n");
		fmt::print("  {} COMMAND [OPTIONS...]\n\n", command);
		
		fmt::print("Available commands:\n");

		for (auto command : commands) {
			std::string commandName = command.first;
			std::string commandDescription = command.second->description();

			fmt::print("  {: <{}}", commandName, 15);
			fmt::print("{}\n", commandDescription);
		}
	}
private:
	std::map<std::string, Command*> commands;
};
