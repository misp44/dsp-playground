#include <iostream>
#include <cxxopts.hpp>
#include <fmt/core.h>
#include <fmt/color.h>
#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#include "commands/commands.hpp"
#include "commands/play.hpp"
#include "commands/generate.hpp"
#include "commands/process.hpp"
#include "commands/listparams.hpp"
#include "commands/impulseresponse.hpp"

void signalHandler(int signal);

int main(int argc, char** argv) {
	signal(SIGSEGV, signalHandler);

	Commands commands;
	commands.addCommand(new PlayCommand());
	commands.addCommand(new GenerateCommand());
	commands.addCommand(new ProcessCommand());
	commands.addCommand(new ListParamsCommand());
	commands.addCommand(new ImpulseResponseCommand());

	commands.execute(argc, argv);
}

void signalHandler(int signal) {
	void *array[10];
	size_t size;

	size = backtrace(array, 10);

	fprintf(stderr, "\n\nError: signal %d:\n", signal);
	backtrace_symbols_fd(array, size, STDERR_FILENO);
	exit(1);
}
