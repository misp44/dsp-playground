#pragma once

inline void convertFloatToInt16(const float* source, signed short* destination, unsigned size) {
	for (unsigned i = 0; i < size; i++) {
		destination[i] = source[i] * 32767;
	}
}

inline void convertInt16ToFloat(const signed short* source, float* destination, unsigned size) {
	for (unsigned i = 0; i < size; i++) {
		destination[i] = ((double)source[i]) / 32767.0;
	}
}
