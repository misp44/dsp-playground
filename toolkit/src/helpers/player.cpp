#include <iostream>
#include <fmt/core.h>
#include <fmt/color.h>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>

#include "player.hpp"

int playAudioFile(const std::string& filename) {
	sf::SoundBuffer buffer;

	if (!buffer.loadFromFile(filename)) {
		fmt::print(fg(fmt::color::crimson), "Cannot open \"{}\" file\n", filename);

		return 1;
	}
	
	sf::Sound sound;
	sound.setBuffer(buffer);
	sound.play();

	while (sound.getStatus() == sf::SoundSource::Status::Playing) {
		sf::sleep(sf::milliseconds(100));

		std::cout << "\33[2K\rTime elapsed " << sound.getPlayingOffset().asSeconds() << std::flush;
	}

	std::cout << "\33[2K\r" << std::flush;

	return 0;
}