#pragma once

#include "kernelloader.hpp"

class ExternalKernel {
	KernelLoader loader;
	dsp::Kernel* kernel;
public:
	/**
	 * @param filename - path to dynamic library, or name of installed kernel
	 */
	ExternalKernel(const std::string& filename) {
		loader.load(filename);

		kernel = loader.create();
	}

	~ExternalKernel() {
		loader.destroy(kernel);
	}

	inline dsp::Kernel* get() {
		return kernel;
	}
};
