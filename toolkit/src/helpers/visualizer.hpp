#pragma once

#include "skia/core/SkData.h"
#include "skia/core/SkSurface.h"
#include "skia/core/SkCanvas.h"
#include "skia/core/SkPath.h"

#include "dspkernel.hpp"

class Visualizer {
	int channels;
	unsigned length;
	int width;
	int height;
	int samplesPerPixel;
	unsigned processed;
	int lastX = 0;
	float min = 1;
	float max = -1;
	float maxInWholeSignal = 0;

	sk_sp<SkSurface> surface;
	SkCanvas* canvas;
	SkPath pathMin;
	SkPath pathMax;

public:
	Visualizer(
		int channels,
		unsigned length,
		int width,
		int height
	):
		channels(channels),
		length(length),
		width(width),
		height(height),
		samplesPerPixel(length / width),
		processed(0) {

		surface = SkSurface::MakeRasterN32Premul(width, height);
		canvas = surface->getCanvas();

		pathMin.moveTo(0, 0);
		pathMax.moveTo(0, 0);
	}

	void process(dsp::Sample* buffer, unsigned size);

	void write(const std::string &filename);

private:
};
