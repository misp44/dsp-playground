#include <iostream>
#include <cstdio>
#include <chrono>
#include <cstring>
#include <fmt/core.h>
#include <fmt/color.h>
#include <SFML/Audio.hpp>

#include "dspkernel.hpp"
#include "arraystools.hpp"
#include "kernelparamsparser.hpp"
#include "kernelprocessor.hpp"
#include "externalkernel.hpp"
#include "analyzer.hpp"
#include "visualizer.hpp"

constexpr int blockSize = 1024;

void KernelProcessor::applyKernelParameters(const std::vector<double>& params) {
	int i = 0;
	int paramsNumber = kernel.get()->getParametersNumber();

	for (auto param : params) {
		if (i >= paramsNumber) {
			fmt::print(fg(fmt::color::crimson), "More parameters then kernel specified\n");

			return;
		}

		fmt::print("  {: >{}}:", i, 5);
		fmt::print(" {: <{}}", kernel.get()->getParameterName(i), 20);
		dsp::ParameterType parameterType = kernel.get()->getParameterType(i);
		fmt::print("({})", dsp::parameterTypeString(parameterType));
		fmt::print(" = {}\n", param);

		switch (parameterType) {
			case dsp::ParameterType::FLOAT:
				kernel.get()->setParameter(i, (float)param);
				break;
			case dsp::ParameterType::DOUBLE:
				kernel.get()->setParameter(i, param);
				break;
		}

		i++;
	}
}

void KernelProcessor::applyKernelParameters(const std::vector<std::string>& params) {
	KernelParamsParser parser(kernel.get());

	parser.apply(params);
}

void KernelProcessor::process(
	int sampleRate,
	int inputChannels,
	int outputChannels,
	unsigned samples,
	sf::InputSoundFile* inputFile,
	const std::string& outputFilename
){
	Visualizer visualizer(outputChannels, samples, 1500, 300);

	sf::OutputSoundFile outputFile;

	outputFile.openFromFile(outputFilename, sampleRate, outputChannels);

	Analyzer analyzer(sampleRate, outputChannels);

	kernel.get()->configurePlayback(sampleRate, inputChannels, outputChannels, blockSize);
	fmt::print("Playback configured. Sample rate: {}, inputChannels: {}, output channels: {}, block size: {}\n", sampleRate, inputChannels, outputChannels, blockSize);
	
	std::unique_ptr<dsp::Sample[]> input(inputFile != nullptr ? new dsp::Sample[blockSize * inputChannels] : nullptr);
	std::unique_ptr<sf::Int16[]> inputInt(inputFile != nullptr ? new sf::Int16[blockSize * inputChannels] : nullptr);

	std::unique_ptr<dsp::Sample[]> output(new dsp::Sample[blockSize * outputChannels]);
	std::unique_ptr<sf::Int16[]> outputInt(new sf::Int16[blockSize * outputChannels]);

	auto start = std::chrono::high_resolution_clock::now();

	for (int i = 0; i < samples; i += blockSize) {
		std::cout << "\33[2K\rProcessed " << i << " samples" << std::flush;

		if (inputFile != nullptr) {
			memset(inputInt.get(), 0, blockSize * inputChannels * sizeof(sf::Int16));
			inputFile->read(inputInt.get(), blockSize * inputChannels);
			convertInt16ToFloat(inputInt.get(), input.get(), blockSize * inputChannels);
		}

		memset(output.get(), 0, blockSize * outputChannels * sizeof(dsp::Sample));

		kernel.get()->process(inputFile ? input.get() : nullptr, output.get());

		if (isAnalyzerEnabled) {
			analyzer.analyze(output.get(), blockSize);
			visualizer.process(output.get(), blockSize);
		}

		convertFloatToInt16(output.get(), outputInt.get(), blockSize * outputChannels);
		outputFile.write(outputInt.get(), blockSize * outputChannels);
	}

	auto stop = std::chrono::high_resolution_clock::now();
	auto processingTime = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count();
	double cpuUsage = (double)processingTime / ((samples / sampleRate) * 1000);

	fmt::print("\33[2K\rProcessing finished in {} ms. Processor usage {}%\n", processingTime, cpuUsage);

	if (isAnalyzerEnabled) {
		visualizer.write("out.png");
		analyzer.printResults();
	}
}

void KernelProcessor::generateImpulseResponse(
	int sampleRate,
	int outputChannels,
	unsigned samples,
	const std::string& outputFilename
) {
	Visualizer visualizer(outputChannels, samples, 1500, 300);

	std::FILE* outputTextFile = fopen(outputFilename.c_str(), "w");

	kernel.get()->configurePlayback(sampleRate, 1, outputChannels, blockSize);
	fmt::print("Playback configured. Sample rate: {}, inputChannels: {}, output channels: {}, block size: {}\n", sampleRate, 1, outputChannels, blockSize);
	
	std::unique_ptr<dsp::Sample[]> input(new dsp::Sample[blockSize]);
	std::unique_ptr<dsp::Sample[]> output(new dsp::Sample[blockSize * outputChannels]);

	memset(input.get(), 0, blockSize * sizeof(dsp::Sample));

	auto start = std::chrono::high_resolution_clock::now();

	fmt::print(outputTextFile, "IR=[");

	for (int i = 0; i < samples; i += blockSize) {
		std::cout << "\33[2K\rProcessed " << i << " samples" << std::flush;

		// Impulse
		if (i == 0) {
			input[0] = 1;
		} else {
			input[0] = 0;
		}

		memset(output.get(), 0, blockSize * outputChannels * sizeof(dsp::Sample));

		kernel.get()->process(input.get(), output.get());
		visualizer.process(output.get(), blockSize);

		// TODO: what is the best output format for gnuplot?
		for (int i = 0; i < blockSize; i++) {
			for (int j = 0; j < outputChannels; j++) {
				fmt::print(outputTextFile, "{} ", output[i * outputChannels + j]);
			}
		}
	}

	fmt::print(outputTextFile, "];\nP=plot(IR);\nwaitfor(P);\n");

	auto stop = std::chrono::high_resolution_clock::now();
	auto processingTime = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count();
	double cpuUsage = (double)processingTime / ((samples / sampleRate) * 1000);

	fmt::print("\33[2K\rProcessing finished in {} ms. Processor usage {}%\n", processingTime, cpuUsage);

	fclose(outputTextFile);

	visualizer.write("out.png");
}
