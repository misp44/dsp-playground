#pragma once

#include "dspkernel.hpp"

class KernelLoader {
	void* handle;

	dsp::Kernel* (*_create)();
	void (*_destroy)(dsp::Kernel*);

public:
	/**
	 * @brief Loads kernel from dynamic library
	 *
	 * The method looks for the kernel in $home/.dsptool/kernels directory.
	 * In such case, only kernel name is needed, e.g. "sine_wave" instead of "libsine_wave.dylib".
	 *
	 * If the file doesn't exist in the installation directory, path is used as a relative path to "dylib" file.
	 *
	 * @param path 
	 */
	void load(const std::string& path);

	dsp::Kernel* create() const;

	void destroy(dsp::Kernel* kernel) const;

private:
	std::string getInstallationDir() const;
};
