#include <dlfcn.h>
#include <unistd.h>
#include <stdexcept>
#include <fmt/color.h>
#include <sys/types.h>
#include <pwd.h>
#include "kernelloader.hpp"

bool fileExists(const std::string& path);

void KernelLoader::load(const std::string& path) {
	const std::string dspToolkitPath = getInstallationDir();
	const std::string installedPath = dspToolkitPath + "/lib" + path + "_kernel.dylib";

	fmt::print("Loading the kernel library");

	handle = dlopen(fileExists(installedPath) ? installedPath.c_str() : path.c_str(), RTLD_LAZY);

	_create = reinterpret_cast<dsp::Kernel* (*)()>(dlsym(handle, "createKernel"));
	_destroy = reinterpret_cast<void (*)(dsp::Kernel*)>(dlsym(handle, "destroyKernel"));

	fmt::print("\33[2K\r");
}

dsp::Kernel* KernelLoader::create() const {
	fmt::print("Creating kernel");

	dsp::Kernel* kernel = _create();

	int version = kernel->dspKernelVersion();

	fmt::print("\33[2K\r");

	if (version != dsp::VERSION) {
		fmt::print(fg(fmt::color::crimson), "Kernel version mismath. Expected {}, got {}\n", dsp::VERSION, version);

		destroy(kernel);

		throw std::runtime_error("Kernel incompatible");
	}

	fmt::print(fg(fmt::color::crimson), "Kernel name \"{}\"\n", kernel->getName());

	return kernel;
}

void KernelLoader::destroy(dsp::Kernel* kernel) const {
	return _destroy(kernel);
}

std::string KernelLoader::getInstallationDir() const {
	struct passwd *pw = getpwuid(getuid());

	std::string homedir = pw->pw_dir;

	return homedir + "/.dsptool/kernels";
}

bool fileExists(const std::string& path) {
	return access(path.c_str(), F_OK) != -1;
}
