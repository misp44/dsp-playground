#include <cmath>
#include <fmt/core.h>
#include <fmt/color.h>

#include "analyzer.hpp"

void Analyzer::analyze(dsp::Sample* buffer, unsigned blockSize) {
	double mono;

	for (int i = 0; i < blockSize; i++) {
		mono = 0;

		for (int channel = 0; channel < channels; channel++) {
			float input = buffer[i * channels + channel];
			mono += input;

			if (input < minAmplitudeValue[channel]) {
				minAmplitudeValue[channel] = input;
			}

			if (input > maxAmplitudeValue[channel]) {
				maxAmplitudeValue[channel] = input;
			}

			integrals[channel] += input * dt;
			squareIntegrals[channel] += input * input;
		}

		mono = mono / (double)channels;
		monoIntegral += mono * dt;
		monoSquareIntegral += mono * mono;

		samples++;
	}
}

inline void printAmplitude(const char* message, double amplitude) {
	if (amplitude > 1 || amplitude < -1) {
		fmt::print(fg(fmt::color::yellow), "  {} {} (signal out of -1, 1 range)\n", message, amplitude);

		return;
	}

	fmt::print("  {} {}\n", message, amplitude);
}

void Analyzer::printResults() {
	fmt::print(fg(fmt::color::blue), "\nAnalyzer results:\n");

	for (int i = 0; i < channels; i++) {
		double rms = RMS(squareIntegrals[i]);
		// level of full scale (-1 to 1):
		//   - sine wave is equal    0 dBFS
		//   - square wave is equal +3 dBFS
		double dBFS = RMS_to_dBFS(rms);

		fmt::print("Channel #{}\n",  i);
		printAmplitude("Minimum amplitude value: ",  minAmplitudeValue[i]);
		printAmplitude("Maxmimum amplitude value:",  maxAmplitudeValue[i]);
		fmt::print("  Integral value:           {}\n",  integrals[i]);
		fmt::print("  RMS levl:                 {}\n",  rms);
		fmt::print("  dBFS level:               {}\n",  dBFS);
	}

	double monoRms = RMS(monoSquareIntegral);
	double monodBFS = RMS_to_dBFS(monoRms);

	fmt::print("Mono:\n");
	fmt::print("  Integral value:           {}\n",  monoIntegral);
	fmt::print("  RMS levl:                 {}\n",  monoRms);
	fmt::print("  dBFS level:               {}\n",  monodBFS);
}

double Analyzer::RMS(long double squareIntegral) {
	return sqrt(squareIntegral / (long double)samples);
}

double Analyzer::RMS_to_dBFS(double rms) {
	constexpr double sqrtOfTwo = 1.4142135623730951;

	return 20 * log10(rms * sqrtOfTwo);
}
