#include <iostream>
#include <string>

#include <fmt/core.h>
#include <fmt/color.h>

#include <cppcodec/base64_rfc4648.hpp>

#include "skia/core/SkImage.h"
#include "skia/core/SkStream.h"
#include "skia/core/SkPaint.h"

#include "visualizer.hpp"

void draw(SkCanvas* rasterCanvas);

// TODO: Add different visualisers
// - waveform (currently implemented)
// - FFT
// - Spectrogram
void Visualizer::process(dsp::Sample* buffer, unsigned size) {
	float halfHeight = (float)height / 2;

	for (int i = 0; i < size; i++) {
		int x = processed / samplesPerPixel;

		if (buffer[i * channels] < min) {
			min = buffer[i * channels];
		}

		if (buffer[i * channels] > max) {
			max = buffer[i * channels];
		}

		if (max > maxInWholeSignal) {
			maxInWholeSignal = max;
		}

		if (-min > maxInWholeSignal) {
			maxInWholeSignal = -min;
		}

		if (lastX < x) {
			// TODO: Multi channel support
			// inverse numbers to fit coordinate system (lower y is on the top)
			pathMax.lineTo(x, -max * halfHeight);
			pathMin.lineTo(x, -min * halfHeight);

			lastX = x;
			min = 1;
			max = -1;
		}

		processed++;
	}
}

void Visualizer::write(const std::string &filename) {
	SkPaint paint;

	paint.setColor(SK_ColorWHITE);

	paint.setAntiAlias(true);
	paint.setStyle(SkPaint::kStroke_Style);
	paint.setStrokeWidth(1);

	canvas->translate(0, ((float)height / 2));
	canvas->scale(1, 1.0f/maxInWholeSignal);
	canvas->drawPath(pathMax, paint);
	canvas->drawPath(pathMin, paint);

	using base64 = cppcodec::base64_rfc4648;

	sk_sp<SkImage> img(surface->makeImageSnapshot());

	if (!img) {
		// TODO: Error

		return;
	}


	sk_sp<SkData> png(img->encodeToData());

	if (!png) {
		// TODO: Error

		return;
	}

	std::string encodedPng = base64::encode((const uint8_t*) png->data(), png->size());

	fmt::print(fg(fmt::color::blue), "\nWaveforms:\n");
	std::cout << "Output channel #0\n";
	fmt::print(fg(fmt::color::white), "{}\n", maxInWholeSignal);
	std::cout << "\033]1337;File=inline=1;preserveAspectRatio=1";
	std::cout << ':' << encodedPng << "\a\n";
	fmt::print(fg(fmt::color::white), "{}\n", -maxInWholeSignal);

	SkFILEWStream out(filename.c_str());

	out.write(png->data(), png->size());
}
