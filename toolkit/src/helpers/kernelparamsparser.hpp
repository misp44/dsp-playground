#pragma once

#include <vector>
#include <map>
#include "dspkernel.hpp"

class KernelParamsParser {
	dsp::Kernel* kernel;
	std::map<std::string, int> idsOfParams;
public:
	KernelParamsParser(dsp::Kernel* kernel): kernel(kernel) {
		for (int id = 0; id < kernel->getParametersNumber(); id++) {
			idsOfParams[kernel->getParameterName(id)] = id;
		}
	}

	/**
	 * @brief Apply kernel params in "-P name=value" format.
	 */
	void apply(const std::vector<std::string>& params);

private:
	/**
	 * @brief Converts params to paramId -> value map.
	 */
	std::map<int, std::string> parse(const std::vector<std::string>& params);

	void applyParameter(dsp::ParameterType parameterType, int id, const std::string& value);
};
