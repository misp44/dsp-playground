#pragma once

#include <vector>
#include "dspkernel.hpp"

class Analyzer {
private:
	double dt;
	int channels;

	std::vector<double> minAmplitudeValue;
	std::vector<double> maxAmplitudeValue;
	std::vector<long double> integrals;
	std::vector<long double> squareIntegrals;

	// Mono measurements
	double monoIntegral = 0;
	long double monoSquareIntegral = 0;
	long samples = 0;

public:
	Analyzer(double sampleRate, int channels): dt(1 / sampleRate), channels(channels) {
		for (int channel = 0; channel < channels; channel++) {
			minAmplitudeValue.push_back(0);
			maxAmplitudeValue.push_back(0);
			integrals.push_back(0);
			squareIntegrals.push_back(0);
		}
	}

	void analyze(dsp::Sample* buffer, unsigned size);
	
	void printResults();

private:
	double RMS(long double squareIntegral);

	double RMS_to_dBFS(double rms);
};
