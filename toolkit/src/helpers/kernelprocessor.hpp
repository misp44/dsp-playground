#pragma once

#include <string>
#include <SFML/Audio.hpp>

#include "externalkernel.hpp"

class KernelProcessor {
	ExternalKernel kernel;
	bool isAnalyzerEnabled = false;

public:
	KernelProcessor(const std::string& kernelPath): kernel(kernelPath) {}

	/**
	 * @deprecated Use const std::vector<string>& params instead
	 * TODO: Remove
	 */
	void applyKernelParameters(const std::vector<double>& params);

	void applyKernelParameters(const std::vector<std::string>& params);

	inline void enableAnalyzer() {
		isAnalyzerEnabled = true;
	}

	void process(
		int sampleRate,
		int inputChannels,
		int outputChannels,
		unsigned samples,
		sf::InputSoundFile* inputFile,
		const std::string& outputFilename
	);

	void generateImpulseResponse(
		int sampleRate,
		int outputChannels,
		unsigned samples,
		const std::string& outputFilename
	);
};
