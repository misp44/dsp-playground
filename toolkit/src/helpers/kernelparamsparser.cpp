#include <string>
#include <sstream>
#include <fmt/core.h>
#include <fmt/color.h>

#include "kernelparamsparser.hpp"

constexpr char arrayDelimiter = ';';

template<class T>
inline void printValue(T value) {
	fmt::print(fg(fmt::color::blue), "{}", value);
}

template<class T>
inline void printArray(const std::vector<T>& vector) {
	fmt::print(fg(fmt::color::blue), "[{}]", fmt::join(vector, ", "));
}

void KernelParamsParser::apply(const std::vector<std::string>& params) {
	std::map<int, std::string> paramsMap = parse(params);

	for (const auto& [id, value] : paramsMap) {
		dsp::ParameterType parameterType = kernel->getParameterType(id);

		fmt::print("  {: >{}}:", id, 5);
		fmt::print(" {: <{}}", kernel->getParameterName(id), 20);
		fmt::print("({})", dsp::parameterTypeString(parameterType));
		fmt::print(" = ");

		applyParameter(parameterType, id, value);

		fmt::print("\n");
	}
}

std::map<int, std::string> KernelParamsParser::parse(const std::vector<std::string>& params) {
	std::map<int, std::string> paramsMap;

	for (std::string param : params) {
		std::istringstream paramStream(param);
		std::string name;
		std::string value;

		std::getline(paramStream, name, '=');
		std::getline(paramStream, value);

		// TODO: Check if param exists

		paramsMap[idsOfParams[name]] = value;
	}

	return paramsMap;
}

void KernelParamsParser::applyParameter(dsp::ParameterType parameterType, int id, const std::string& value) {
	switch (kernel->getParameterType(id)) {
		case dsp::ParameterType::FLOAT: {
			float parsed = std::stof(value);
			printValue(parsed);
			kernel->setParameter(id, parsed);
			break;
		}
		
		case dsp::ParameterType::DOUBLE: {
			double parsed = std::stod(value);
			printValue(parsed);
			kernel->setParameter(id, parsed);
			break;
		}
		
		case dsp::ParameterType::FLOAT_ARRAY: {
			std::vector<float> vector;
			std::istringstream paramStream(value);
			
			for (std::string arrayElement; std::getline(paramStream, arrayElement, arrayDelimiter); ) {
				vector.push_back(std::stof(arrayElement));
			}

			printArray(vector);
			kernel->setParameter(id, dsp::Array<float>(vector.size(), vector.data()));
			break;
		}

		case dsp::ParameterType::DOUBLE_ARRAY: {
			std::vector<double> vector;
			std::istringstream paramStream(value);
			
			for (std::string arrayElement; std::getline(paramStream, arrayElement, arrayDelimiter); ) {
				vector.push_back(std::stod(arrayElement));
			}

			printArray(vector);
			kernel->setParameter(id, dsp::Array<double>(vector.size(), vector.data()));
			break;
		}
		
		default:
			fmt::print(fg(fmt::color::crimson), "unknown type");
	}
}

