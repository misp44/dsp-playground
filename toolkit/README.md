# Description

CLI toolkit is a tool for testing DSP kernels.

# Install dependencies

```sh
../vcpkg/vcpkg install
```

# Build

```sh
cmake -B build -S .
cmake --build build
```

# Install

```
./install.sh
```

Add export to your shell startup file (eg. `~/.bashrc` or `~/.zshrc`):
```
export PATH=$PATH:$HOME/.dsptool/bin
```

The tool will be available in new shell session. If you want to get access immediately, use:
```
source ~/.[your rc file]
```

# Usage

```
dsptool
```

```
DSP playground toolkit
Usage:
  dsptool COMMAND [OPTIONS...]

Available commands:
  generate       Generates audio with specified kernel
  listparams     Get list of kernel parameters
  play           Play audio file
  process        Process audio with specified effect kernel
```

# Debugging

## On Linux

```
cmake -B build -S . -DCMAKE_BUILD_TYPE=Debug

ASAN_OPTIONS=detect_leaks=1 ./build/dsptool
```

## On MacOS

To build with debug options on MacOS, you need to install `clang` compiler from `brew` first - Apple version doesn't have sanitize tools.

```
./mac_build_debug.sh

cmake --build build

ASAN_OPTIONS=detect_leaks=1 ./build/dsptool
```


