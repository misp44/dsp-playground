# DSP Playground

This repository contains various tools to help you create your own sound effects and instruments.

# Requirements

* c++ compiler
* vcpkg
* Linux/Mac (Windows not supported)

# Prepare vcpkg

```sh
git clone https://github.com/microsoft/vcpkg.git
cd vcpkg
./bootstrap-vcpkg.sh
cd ..
```

# Projects

- [CLI toolkit](./toolkit/README.md)
- [Sine wave generator kernel](./kernels/sine-wave/README.md)
- [Additive wave generator kernel](./kernels/additive-sines/README.md)
- [Delay kernel](./kernels/delay/README.md)
- [Delay kernel (in Vult language)](./kernels/vult-delay/README.md)
- [Schroeder reverb](./kernels/schroeder-reverb/README.md)

